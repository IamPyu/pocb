#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include "pocb.h"

// Mallocator (Malloc)
static pocb_pointer_t mallocator_alloc(pocb_pointer_t sp, u64 size) {
  return malloc(size);
}

static void mallocator_free(pocb_pointer_t sp, pocb_pointer_t ptr) {
  free(ptr);
}

struct pocb_mallocator pocb_new_mallocator() {
  struct pocb_mallocator a = {
    .alloc = mallocator_alloc,
    .free = mallocator_free
  };
  return a;
}

// Rallocator (Arena/Region Allocator)
static pocb_pointer_t rallocator_alloc(void *sp, u64 size) {
  struct pocb_rallocator *self = sp;
  if (self->size + size > self->capacity) {
    self->capacity *= 2;
    self->data = realloc(self->data, self->capacity);
  }

  pocb_pointer_t data = &self->data[self->size];
  self->size += size;
  return data;
}

static void rallocator_free(pocb_pointer_t sp, pocb_pointer_t ptr) {
  struct pocb_rallocator *self = sp;
  self->capacity = 0;
  self->size = 0;
  free(self->data);
  *self = (struct pocb_rallocator){0};
}

struct pocb_rallocator pocb_new_rallocator(u64 size) {
  struct pocb_rallocator a = {
    .alloc = rallocator_alloc,
    .free = rallocator_free, 
    .data = malloc(sizeof(pocb_pointer_t*) * size),
    .size = 0,
    .capacity = size
  };
  return a;
}

// C Strings

char *pocb_cstrdup(const char *str, u64 len) {
  struct pocb_mallocator allocator = pocb_new_mallocator(pocb_false);
  char *s = allocator.alloc(&allocator, len);
  
  for (u64 i = 0; i < len; i++) {
    s[i] = str[i];
  }

  return s;
}

char *pocb_cstrcpy(char *dst, const char *src, u64 len) {
  for (u64 i = 0; i < len; i++) {
    dst[i] = src[i];
    if (src[i] == '\0') break;
  }
  return dst;
}

// POCB Strings

struct pocb_string pocb_strcnew(char *str) {
  u64 len = strlen(str);
  struct pocb_string s = {
    .arr = (u8*)pocb_cstrdup(str, len),
    .len = len
  };
  return s;
}

struct pocb_string pocb_strnew(u8 *arr, u64 len) {
  struct pocb_string s = {
    .arr = arr,
    .len = len
  };
  return s;
}

struct pocb_string pocb_strcat(const struct pocb_string *s1, const struct pocb_string *s2) {
  char *cs1;
  char *cs2;

  cs1 = pocb_cstrdup((char*)s1->arr, s1->len);
  cs2 = pocb_cstrdup((char*)s2->arr, s2->len);

  char *str = strncat(cs1, cs2, s1->len + s2->len);
  
  struct pocb_string s = {
    .arr = (u8*)str,
    .len = strlen(str)
  };
  return s;
}

struct pocb_string pocb_strdup(const struct pocb_string *_s) {
  struct pocb_string s = {
    .arr = (u8*)pocb_cstrdup((char*)_s->arr, _s->len),
    .len = _s->len
  };
  return s;
}

// Data Structures

/// Stack

struct pocb_stack pocb_stack_new(i64 capacity, struct pocb_mallocator *allocator) {
  struct pocb_stack s = {
    .capacity = capacity,
    .top = -1,
    .data = allocator->alloc(allocator, capacity * sizeof(pocb_pointer_t))
  };
  return s;
}

void pocb_stack_destroy(struct pocb_stack *stack, struct pocb_mallocator *allocator) {
  allocator->free(allocator, stack->data);
  stack->data = POCB_NULL;
  *stack = (struct pocb_stack){0};
}

pocb_bool_t pocb_stack_full(const struct pocb_stack *stack) {
  return stack->top == stack->capacity;
}

pocb_bool_t pocb_stack_empty(const struct pocb_stack *stack) {
  return stack->top == -1;
}

pocb_bool_t pocb_stack_push(struct pocb_stack *stack, pocb_pointer_t item) {
  if (pocb_stack_full(stack)) return pocb_true;
  stack->data[++stack->top] = item;
  return pocb_false;
}

pocb_pointer_t pocb_stack_pop(struct pocb_stack *stack) {
  if (pocb_stack_empty(stack)) return POCB_NULL;
  return stack->data[stack->top--];
}

pocb_pointer_t pocb_stack_peek(const struct pocb_stack *stack) {
  if (pocb_stack_empty(stack)) return POCB_NULL;
  return stack->data[stack->top];
}

/// String Hashmap

u64 pocb_hash(const struct pocb_string str, const u64 max_size) {
  u64 hashed = 0;

  for (u64 i = 0; i < str.len; i++) {
    hashed += str.arr[i];
    hashed *= str.arr[i] != 0 ? str.arr[i] : 2;
    hashed = (hashed * str.arr[i]) % max_size;
  }

  return hashed;
}

// Logging

void pocb_log(enum pocb_logging_level lvl, const char *fmt, ...) {
  switch (lvl) {
  case POCB_INFO:
    fprintf(stderr, "INFO: ");
    break;
  case POCB_WARN:
    fprintf(stderr, "WARN: ");
    break;
  case POCB_ERROR:
    fprintf(stderr, "ERROR: ");
    break;
  case POCB_FATAL:
    fprintf(stderr, "FATAL: ");
    break;
  default: break;
  }

  va_list vargs;
  va_start(vargs, fmt);
  vfprintf(stderr, fmt, vargs);
  va_end(vargs);

  if (lvl == POCB_FATAL) {
    exit(1);
  }
}
