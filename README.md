# POCB

Codebase for C99.

Planned Features:

- [X] Helper Macros (lengthof, assert, max, min)
- [X] Easier & Safer Memory Management
- [X] Safer Strings
- [X] Data Structures

## Drawbacks

- I do not plan to add any sort of Microsoft Windows support. Though some features may work.

## Credits

This codebase was made following along [this](https://www.youtube.com/playlist?list=PLT6InxK-XQvNKTyLXk6H6KKy12UYS_KDL) series and with extra things I added aswell.

It's really good and you should follow along it if you want to make your own codebase.
