include config.mk

SRC = pocb.c util.c 
OBJS = $(SRC:.c=.o)
CFLAGS += -std=c99 -Wall -Wextra -fPIC -MD

.PHONY: all install uninstall test clean
all: libpocb.so libpocb.a
libpocb.so: $(OBJS)
	$(CC) -shared $(OBJS) $(CFLAGS) -o libpocb.so

libpocb.a: $(OBJS)
	ar rcs libpocb.a $(OBJS)

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

install: all
	cp libpocb.so $(PREFIX)/lib
	cp libpocb.a $(PREFIX)/lib
	cp pocb.h $(PREFIX)/include

uninstall:
	rm $(PREFIX)/lib/libpocb.so
	rm $(PREFIX)/lib/libpocb.a
	rm $(PREFIX)/include/pocb.h

test: libpocb.a
	$(CC) $(CFLAGS) -g test.c -o test -L. -l:libpocb.a
	./test || gdb ./test

clean:
	-rm test
	-rm libpocb.so
	-rm *.o
