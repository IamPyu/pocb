#include <stdio.h>
#include "pocb.h"

void hashmap();
void rallocator();

int main() {
  hashmap();

  return 0;
}

void hashmap() {
  int grades[50] = {};
  
  pocb_hashmap_set(grades, pocb_strcnew("dan"), 9);
  pocb_hashmap_set(grades, pocb_strcnew("lexi"), 6);
  pocb_hashmap_set(grades, pocb_strcnew("joe"), 3);
  pocb_hashmap_set(grades, pocb_strcnew("eva"), 10);
  
  printf("Dan's grade: %d\n", pocb_hashmap_get(grades, pocb_strcnew("dan")));
  printf("Lexi's grade: %d\n", pocb_hashmap_get(grades, pocb_strcnew("lexi")));
  printf("Joe's grade: %d\n", pocb_hashmap_get(grades, pocb_strcnew("joe")));
  printf("Eva's grade: %d\n", pocb_hashmap_get(grades, pocb_strcnew("eva"))); 
}

void rallocator() {
  struct pocb_rallocator allocator = pocb_new_rallocator(500);

  u8 *arr = allocator.alloc(&allocator, sizeof(u8) * 4);

  arr[0] = 1;
  arr[1] = 2;
  arr[2] = 3;
  arr[3] = 4;

  printf("Before free: %d\n", arr[2]);
  
  allocator.free(&allocator, POCB_NULL);

  printf("After free: %d\n", arr[2]);
}
