#ifndef POCB_H
#define POCB_H

/*
  Pyu's Own Code Base (POCB)
  Codebase for C99

  Naming scheme:
  snake_case everything
*/

// Older compilers (K&R) do not support void pointers so we have to create a custom pointer type
#if defined(__STDC__) || defined(__cplusplus) || defined(_MSC_EXTENSIONS)
typedef void *pocb_pointer_t; // POSIX C is used, we can use void pointers.
#else
typedef char *pocb_pointer_t; // K&R compiler is used, we have to use char pointers as void pointers.
#endif

// Helper Macros

#define pocb_stringify(s) (#s)
#define pocb_glue(x,y) (x##y)

#define pocb_pow(T,b,e) ({                      \
      T res = 1;                                \
      for (T i = 0; i < (e); ++i) res *= (b);   \
      res;                                      \
  })

// Lengthof Macro
#define pocb_lengthof(x) (sizeof(x) / sizeof(*x))

// Max and Min Macros
#define pocb_max(x, y) ((x) > (y) ? (x) : (y)) // Max
#define pocb_min(x, y) ((x) < (y) ? (x) : (y)) // Min

// Constants
#define POCB_NULL ((void*)0)
#define POCB_EOF (-1)

// Basic Primitive Types
#include <stdint.h>

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float f32;
typedef double f64;

#define MIN_I8 (-(pocb_pow(i8, 2, 7)-1))
#define MAX_I8 (-MIN_I8)
#define MIN_I16 (-(pocb_pow(i16, 2, 15)-1))
#define MAX_I16 (-MIN_I16)
#define MIN_I32 (-(pocb_pow(i32, 2, 31)-1))
#define MAX_I32 (-MIN_I32)
#define MIN_I64 (-(pocb_pow(i64, 2, 63)-1))
#define MAX_I64 (-MIN_I64)

#define MAX_U8 (pocb_pow(u8, 2, 8)-1)
#define MAX_U16 (pocb_pow(u16, 2, 16)-1)
#define MAX_U32 (pocb_pow(u32, 2, 32)-1)
#define MAX_U64 (pocb_pow(u64, 2, 64)-1)

// Automatically include commonly used libc and libm headers and add features for them

#ifdef POCB_LIBC
// Header descriptions are from their repspective man-pages

#include <stdio.h> // Standard Buffered Input/Output
#include <stdlib.h> // Standard Library Definitions
#include <stdbool.h> // Boolean Type and Values
#include <unistd.h> // Standard Symbolic Constants and Types
#include <getopt.h> // Command line option parsing
#include <stddef.h> // Standard Type Definitions
#include <math.h> // Mathematical Declarations
#include <time.h> // Time Types
#include <fcntl.h> // File Control Operations
#include <termios.h> // Define Values for Termios
#include <string.h> // String Operations
#include <limits.h> // Implementation-Defined Constants
#include <glob.h> // Pathname Pattern-Matching Types
#include <errno.h> // System Error Numbers
#include <dlfcn.h> // Dynamic Linking
#include <dirent.h> // Format of Directory Entries
#include <assert.h> // Verify Program Assertion
#include <sys/stat.h> // Data Returned by the stat() Function

#define pocb_eval_print(x, f) do { printf("%s = %" (f) "\n", #x, (x)); } while (0)
#endif

// Boolean Type

enum pocb_bool { pocb_true = 1, pocb_false = 0 };
typedef enum pocb_bool pocb_bool_t;

// Allocators

typedef pocb_pointer_t pocb_allocator_alloc_func(pocb_pointer_t sp, u64 size);
typedef void pocb_allocator_free_func(pocb_pointer_t sp, pocb_pointer_t ptr);

// Base class for allocators
struct pocb_allocator {
  pocb_allocator_alloc_func *alloc;
  pocb_allocator_free_func *free;
};

// libc allocator (malloc).
// mallocator stands for Malloc Allocator
struct pocb_mallocator {
  pocb_allocator_alloc_func *alloc;
  pocb_allocator_free_func *free;
};
struct pocb_mallocator pocb_new_mallocator();

// simple arena/region allocator, no fancy stuff, just uses malloc under the hood.
// rallocator stands for Region Allocator
struct pocb_rallocator {
  pocb_pointer_t *data;
  u64 size;
  u64 capacity;
  pocb_allocator_alloc_func *alloc;
  pocb_allocator_free_func *free;
};
struct pocb_rallocator pocb_new_rallocator(u64 size);

// Strings

// POCB Implementation of strndup(3). This allocates a new string using a mallocator, remember to free the memory!
char *pocb_cstrdup(const char *str, u64 len);

// POCB Implementation of strncpy(3).
char *pocb_cstrcpy(char *dst, const char *src, u64 len);

struct pocb_string {
  u8 *arr;
  u64 len;
};

// Creates a new POCB String from a C String
struct pocb_string pocb_strcnew(char *str);

// Creates a new POCB String from a character array
struct pocb_string pocb_strnew(u8 *arr, u64 len);

// Concatenates 2 POCB Strings
struct pocb_string pocb_strcat(const struct pocb_string *s1, const struct pocb_string *s2);

// Duplicates a POCB String into a newly (m)allocated string. 
struct pocb_string pocb_strdup(const struct pocb_string *_s);

// Stack

struct pocb_stack {
  i64 top;
  i64 capacity;
  pocb_pointer_t *data;
};

// Create a new stack
struct pocb_stack pocb_stack_new(i64 capacity, struct pocb_mallocator *allocator);
// Destroy a stack
void pocb_stack_destroy(struct pocb_stack *stack, struct pocb_mallocator *allocator);

// Check if a stack is full
pocb_bool_t pocb_stack_full(const struct pocb_stack *stack);
// Check if a stack is empty
pocb_bool_t pocb_stack_empty(const struct pocb_stack *stack);

// Push item onto stack, return pocb_true on error
pocb_bool_t pocb_stack_push(struct pocb_stack *stack, pocb_pointer_t item);

// Pop item from stack, return pointer to stack item, return POCB_NULL on error
pocb_pointer_t pocb_stack_pop(struct pocb_stack *stack);

// Peek item on top of stack, return pointer to stack item, return POCB_NULL on error
pocb_pointer_t pocb_stack_peek(const struct pocb_stack *stack);

// String Hashmap

// Simple hash function
u64 pocb_hash(const struct pocb_string str, const u64 max_size);
// Since Hashmaps are just Arrays under the hood and do not require any other functions we will just use macros to interact with them.

// Set a value of a hashmap
#define pocb_hashmap_set(hashmap, pocb_str, value) do { (hashmap)[pocb_hash((pocb_str), pocb_lengthof((hashmap)))] = value; } while (0)
// Get a value of a hashmap
#define pocb_hashmap_get(hashmap, pocb_str) ({(hashmap)[pocb_hash((pocb_str), pocb_lengthof((hashmap)))];})

// Dynamic Array

struct pocb_array {
  u64 len;
  u64 capacity;
  pocb_pointer_t *data;
};

struct pocb_array pocb_array_new();

// Logging

/// Log level
enum pocb_logging_level { POCB_INFO, POCB_WARN, POCB_ERROR, POCB_FATAL };

/// Logging function
void pocb_log(enum pocb_logging_level lvl, const char *fmt, ...);

#endif
